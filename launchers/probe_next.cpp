#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Camera.h"
#include "Window.h"
#include "Draw_FirstProbe.h"
#include "Draw_House.h"

const GLuint WIDTH = 1024, HEIGHT = 600;

int main() {

  // Продолжение https://habr.com/ru/articles/324968/

  if (!glfwInit()) {
    std::cerr << "e140b6Os33 :: Error in glfwInit()" << std::endl;
    return -1;
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

  Window window;

  {
    window.Create(WIDTH, HEIGHT, "Изучение графики OpenGL 4.5");

    window.MakeCurrentContext();

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
      std::cerr << "WDR81ow9k1 :: Failed to Initialize GLEW" << std::endl;
      return -1;
    }

    Draw_FirstProbe draw_FirstProbe;
    Draw_House draw_House;
    draw_FirstProbe.Initialize();
    draw_House.Initialize();

    Camera camera;
    camera.LookAt(glm::vec3(0.0f, 0.0f, 3.0f),
                  glm::vec3(0.0f, 0.0f, 0.0f),
                  glm::vec3(0.0f, 1.0f, 0.0f));

    window.AssociateWithCamera(&camera);
    
    window.UpdateViewportSize();

    std::cout << "lbn075Bu1k :: GL_VERSION: " << glGetString(GL_VERSION) << std::endl;

    draw_FirstProbe.ResetBuffers();
    draw_House.ResetBuffers();

    GLint nrAttributes;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
    std::cout << "99BvNVD4Nm :: Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

    glEnable(GL_DEPTH_TEST);

    while (!window.ShouldClose()) {
      glfwPollEvents();

      glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      draw_FirstProbe.Render(&camera);
      draw_House.Render(&camera);

      window.SwapBuffers();
    }
  }

  glfwTerminate();

  return 0;

}
