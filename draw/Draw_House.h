//
// Created by pompei on 30.09.23.
//

#ifndef OPENGL_PROBE_DRAW_HOUSE_H
#define OPENGL_PROBE_DRAW_HOUSE_H


#include "Draw.h"

//   https://habr.com/ru/articles/350782/

class Draw_House : public Draw {
private:

  Shader *m_shader;

  GLuint VAO, VPO;

  GLsizei point_count;

  GLint location_R;
  GLint location_N1;
  GLint location_N2;

  GLint location_Model;
  GLint location_View;
  GLint location_Projection;
  
  void deleteBuffers();

public:

  Draw_House() : m_shader(nullptr),
                 VAO(0),
                 VPO(0),
                 point_count(0),
                 location_R(0),
                 location_N1(0),
                 location_N2(0),
                 location_Model(0),
                 location_View(0),
                 location_Projection(0) {}

  ~Draw_House() override;

  void Initialize() override;

  void ResetBuffers() override;

  void Render(Camera *camera) override;
};


#endif //OPENGL_PROBE_DRAW_HOUSE_H
