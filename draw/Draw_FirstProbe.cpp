//
// Created by pompei on 24.09.23.
//

#include "Draw_FirstProbe.h"
#include "util.h"
#include <GLFW/glfw3.h>

#include <GL/glew.h> // Подключаем glew для того, чтобы получить все необходимые заголовочные файлы OpenGL

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


extern "C" const GLchar shaders_prism_1_vertex_glsl[];
extern "C" const GLchar shaders_prism_3_fragment_glsl[];

void Draw_FirstProbe::Initialize() {
  m_shader = new Shader(shaders_prism_1_vertex_glsl, nullptr, shaders_prism_3_fragment_glsl);
  wood_container = Texture::get_wood_container();
  awesome_face = Texture::get_awesome_face();
  ourColorLocation = m_shader->GetUniformLocation("ourColor");
  startTime = (GLfloat) glfwGetTime();

  location_Model = m_shader->GetUniformLocation("model");
  location_View = m_shader->GetUniformLocation("view");
  location_Projection = m_shader->GetUniformLocation("projection");
}

void Draw_FirstProbe::ResetBuffers() {
  deleteBuffers();

  //@formatter:off
  GLfloat vertices[] = {

      //             Позиции                      Цвета                Текстурные координаты
      /* 00 */       4.0f,  7.0f,  0.0f,          1.0f, 0.0f, 0.0f,    1.000f, 1.0f,
      /* 01 */       2.0f,  7.0f,  3.0f,          0.0f, 1.0f, 0.0f,    0.667f, 1.0f,
      /* 02 */      -2.0f,  7.0f,  3.0f,          1.0f, 1.0f, 1.0f,    0.333f, 1.0f,
      /* 03 */      -4.0f,  7.0f,  0.0f,          0.0f, 0.0f, 1.0f,    0.000f, 1.0f,
      /* 04 */      -2.0f,  7.0f, -3.0f,          0.0f, 1.0f, 1.0f,    0.333f, 1.0f,
      /* 05 */       2.0f,  7.0f, -3.0f,          1.0f, 0.0f, 1.0f,    0.666f, 1.0f,
      /* 06 */       4.0f, -7.0f,  0.0f,          0.0f, 1.0f, 1.0f,    1.000f, 0.0f,
      /* 07 */       2.0f, -7.0f,  3.0f,          0.0f, 0.0f, 1.0f,    0.667f, 0.0f,
      /* 08 */      -2.0f, -7.0f,  3.0f,          1.0f, 0.0f, 0.0f,    0.333f, 0.0f,
      /* 09 */      -4.0f, -7.0f,  0.0f,          1.0f, 1.0f, 0.0f,    0.000f, 0.0f,
      /* 10 */      -2.0f, -7.0f, -3.0f,          0.0f, 0.0f, 0.0f,    0.333f, 0.0f,
      /* 11 */       2.0f, -7.0f, -3.0f,          1.0f, 1.0f, 0.0f,    0.667f, 0.0f,

  };
  //@formatter:on

  //@formatter:off
  GLuint indices[] = {
      0,  6,  7, 0,  7, 1,
      1,  7,  8, 1,  8, 2,
      2,  8,  9, 2,  9, 3,
      3,  9, 10, 3, 10, 4,
      4, 10, 11, 4, 11, 5,
      5, 11,  6, 5,  6, 0,
  };
  //@formatter:on

  indices_count = sizeof(indices);

  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);

  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  const int index_position = 0;
  const int index_color = 1;
  const int index_tex_coord = 2;

  glEnableVertexAttribArray(index_position);
  glEnableVertexAttribArray(index_color);
  glEnableVertexAttribArray(index_tex_coord);

  //@formatter:off
  glVertexAttribPointer(index_position,  3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), GLfloat_OFFSET(0));
  glVertexAttribPointer(index_color,     3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), GLfloat_OFFSET(3));
  glVertexAttribPointer(index_tex_coord, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), GLfloat_OFFSET(6));
  //@formatter:on

  glBindVertexArray(0);
}

void Draw_FirstProbe::deleteBuffers() {
  if (VAO) {
    glDeleteVertexArrays(1, &VAO);
    VAO = 0;
  }
  if (VBO) {
    glDeleteBuffers(1, &VBO);
    VBO = 0;
  }
  if (EBO) {
    glDeleteBuffers(1, &EBO);
    EBO = 0;
  }
}

void Draw_FirstProbe::Render(Camera *camera) {

  auto timeValue = ((GLfloat) glfwGetTime() - startTime);
  GLfloat top = (sinf(2 * timeValue) / 2.0f) + 0.5f;

  m_shader->Use();

  glUniform4f(ourColorLocation, top, top, top, 1.0f);

  {
    glm::mat4 model(1.0f);
    model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
    model = glm::rotate(model, 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, 0.3f * timeValue, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, 1.3f * timeValue, glm::vec3(0.0f, 0.0f, 1.0f));

    glUniformMatrix4fv(location_Model, 1, GL_FALSE, glm::value_ptr(model));
  }

  glUniformMatrix4fv(location_View, 1, GL_FALSE, glm::value_ptr(camera->GetView()));
  glUniformMatrix4fv(location_Projection, 1, GL_FALSE, glm::value_ptr(camera->GetProjection()));

  wood_container->Bind(m_shader, 0, "ourTexture1");
  awesome_face->Bind(m_shader, 1, "ourTexture2");

  glBindVertexArray(VAO);
  glDrawElements(GL_TRIANGLES, indices_count, GL_UNSIGNED_INT, 0); // NOLINT(*-use-nullptr)
  glBindVertexArray(0);

}

Draw_FirstProbe::~Draw_FirstProbe() {
  deleteBuffers();
  delete m_shader;
}
