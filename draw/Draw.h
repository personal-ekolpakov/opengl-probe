//
// Created by pompei on 24.09.23.
//

#ifndef OPENGL_PROBE_DRAW_H
#define OPENGL_PROBE_DRAW_H


#include "Shader.h"
#include "Camera.h"

class Draw {
protected:


public:
  Draw() = default;

  virtual ~Draw() = default;

  virtual void Initialize() = 0;

  virtual void ResetBuffers() = 0;

  virtual void Render(Camera *camera) = 0;
};


#endif //OPENGL_PROBE_DRAW_H
