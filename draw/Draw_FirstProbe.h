//
// Created by pompei on 24.09.23.
//

#ifndef OPENGL_PROBE_DRAW_FIRST_PROBE_H
#define OPENGL_PROBE_DRAW_FIRST_PROBE_H

#include "Draw.h"
#include "Texture.h"

class Draw_FirstProbe : public Draw {
private:
  GLuint VBO, VAO, EBO;

  Shader *m_shader;

  Texture *wood_container, *awesome_face;

  GLint ourColorLocation;

  GLfloat startTime;

  GLsizei indices_count;

  GLint location_Model;
  GLint location_View;
  GLint location_Projection;

  void deleteBuffers();

public:
  Draw_FirstProbe() : VBO(0),
                      VAO(0),
                      EBO(0),
                      m_shader(nullptr),
                      wood_container(nullptr),
                      awesome_face(nullptr),
                      ourColorLocation(0),
                      startTime(0),
                      indices_count(0),
                      location_Model(0),
                      location_View(0),
                      location_Projection(0) {}

  ~Draw_FirstProbe() override;

  void Initialize() override;

  void ResetBuffers() override;

  void Render(Camera *camera) override;
};

#endif //OPENGL_PROBE_DRAW_FIRST_PROBE_H
