//
// Created by pompei on 30.09.23.
//

#include "Draw_House.h"
#include "util.h"

extern "C" const GLchar shaders_house_1_vertex_glsl[];
extern "C" const GLchar shaders_house_2_geo_glsl[];
extern "C" const GLchar shaders_house_3_fragment_glsl[];

void Draw_House::Initialize() {
  m_shader = new Shader(shaders_house_1_vertex_glsl, shaders_house_2_geo_glsl, shaders_house_3_fragment_glsl);

  location_R = m_shader->GetUniformLocation("R");
  location_N1 = m_shader->GetUniformLocation("N1");
  location_N2 = m_shader->GetUniformLocation("N2");

  location_Model = m_shader->GetUniformLocation("model");
  location_View = m_shader->GetUniformLocation("view");
  location_Projection = m_shader->GetUniformLocation("projection");
}

void Draw_House::ResetBuffers() {
  deleteBuffers();

  float points[] = {
      +0.0f, +0.5f,
      -0.5f, +0.5f, // верхняя-левая
      +0.5f, +0.5f, // верхняя-правая
      +0.5f, -0.5f, //  нижняя-правая
      -0.5f, -0.5f, //  нижняя-левая
  };

  point_count = sizeof(points) / 2;

  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VPO);

  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VPO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

  const int index_pointVec2 = 0;

  glEnableVertexAttribArray(index_pointVec2);
  glVertexAttribPointer(index_pointVec2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), GLfloat_OFFSET(0));

  glBindVertexArray(0);
}

void Draw_House::Render(Camera *camera) {
  m_shader->Use();

  {
    glm::mat4 model(1.0f);
    model = glm::translate(model, glm::vec3(1.0f, -1.0f, -1.0f));
    model = glm::rotate(model, 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));

    glUniformMatrix4fv(location_Model, 1, GL_FALSE, glm::value_ptr(model));
  }

  glUniformMatrix4fv(location_View, 1, GL_FALSE, glm::value_ptr(camera->GetView()));
  glUniformMatrix4fv(location_Projection, 1, GL_FALSE, glm::value_ptr(camera->GetProjection()));

  glUniform1f(location_R, 0.2f);
  glUniform1ui(location_N1, 17);
  glUniform1ui(location_N2, 7);

  glBindVertexArray(VAO);
  glDrawArrays(GL_POINTS, 0, point_count); // NOLINT(*-use-nullptr)
  glBindVertexArray(0);
}

void Draw_House::deleteBuffers() {
  if (VAO) {
    glDeleteVertexArrays(1, &VAO);
    VAO = 0;
  }

  if (VPO) {
    glDeleteVertexArrays(1, &VPO);
    VPO = 0;
  }
}

Draw_House::~Draw_House() {
  deleteBuffers();
  delete m_shader;
}
