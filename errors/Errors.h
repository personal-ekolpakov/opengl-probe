//
// Created by pompei on 16.09.23.
//

#ifndef OPENGL_PROBE_ERRORS_H
#define OPENGL_PROBE_ERRORS_H
#include <GL/glew.h>
#include <iostream>

class Errors {
public:
  static void checkGlError(const char *placeId);
};


#endif //OPENGL_PROBE_ERRORS_H
