//
// Created by pompei on 16.09.23.
//

#include "Errors.h"

void Errors::checkGlError(const char *placeId) {
  GLenum errorCode = glGetError();
  if (errorCode == GL_NO_ERROR) return;

  const GLubyte *glErrorMessage = gluErrorString(errorCode);

  std::cerr << placeId << " :: GL ERROR " << errorCode << " : " << glErrorMessage << std::endl;
  exit(34);
}
