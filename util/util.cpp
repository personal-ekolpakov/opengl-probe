//
// Created by pompei on 24.09.23.
//

#include <GLFW/glfw3.h>
#include "util.h"

std::string mouseButton_ToStr(int mouseButton) {
  //@formatter:off
  switch (mouseButton) {
    case GLFW_MOUSE_BUTTON_1: return "GLFW_MOUSE_BUTTON_1";
    case GLFW_MOUSE_BUTTON_2: return "GLFW_MOUSE_BUTTON_2";
    case GLFW_MOUSE_BUTTON_3: return "GLFW_MOUSE_BUTTON_3";
    case GLFW_MOUSE_BUTTON_4: return "GLFW_MOUSE_BUTTON_4";
    case GLFW_MOUSE_BUTTON_5: return "GLFW_MOUSE_BUTTON_5";
    case GLFW_MOUSE_BUTTON_6: return "GLFW_MOUSE_BUTTON_6";
    case GLFW_MOUSE_BUTTON_7: return "GLFW_MOUSE_BUTTON_7";
    case GLFW_MOUSE_BUTTON_8: return "GLFW_MOUSE_BUTTON_8";
    default:                  return "UNKNOWN";
  }
  //@formatter:on
}

std::string modifiersKeys_ToStr(int mods) {
  std::string ret;

  if (mods & GLFW_MOD_CONTROL) {
    ret.append("CTRL");
  }

  if (mods & GLFW_MOD_SHIFT) {
    if (!ret.empty()) {
      ret += "+";
    }
    ret.append("SHIFT");
  }

  if (mods & GLFW_MOD_ALT) {
    if (!ret.empty()) {
      ret += "+";
    }
    ret.append("ALT");
  }

  if (mods & GLFW_MOD_SUPER) {
    if (!ret.empty()) {
      ret += "+";
    }
    ret.append("SUPER");
  }

  if (mods & GLFW_MOD_CAPS_LOCK) {
    if (!ret.empty()) {
      ret += "+";
    }
    ret.append("CAPS_LOCK");
  }

  if (mods & GLFW_MOD_NUM_LOCK) {
    if (!ret.empty()) {
      ret += "+";
    }
    ret.append("NUM_LOCK");
  }

  return ret.empty() ? "NO_MODS" : ret;
}

std::string eventAction_ToStr(int action) {
  //@formatter:off
  switch (action) {
    case GLFW_PRESS   : return "PRESS";
    case GLFW_RELEASE : return "RELEASE";
    default           : return "UNKNOWN";
  }
  //@formatter:on
}

bool camera_rotate_right(glm::vec3 &m_eye, glm::vec3 &m_center, glm::vec3 &m_up, GLfloat angle) {
  glm::vec3 dir = m_center - m_eye;

  if ((dir.x == 0 && dir.y == 0 && dir.z == 0) || (m_up.x == 0 && m_up.y == 0 && m_up.z == 0)) {
    return false;
  }

  glm::vec3 toRight = cross(dir, m_up);

  dir /= glm::length(dir);
  toRight /= glm::length(toRight);

  glm::vec3 dirNew = dir * cosf(angle) + toRight * sinf(angle);

  GLfloat distance = glm::distance(m_center, m_eye);

  glm::vec3 upNew = cross(toRight, dirNew);

  m_center = m_eye + dirNew * distance;
  m_up = upNew;

  return true;
}

bool camera_rotate_up(glm::vec3 &m_eye, glm::vec3 &m_center, glm::vec3 &m_up, GLfloat angle) {

  glm::vec3 dir = m_center - m_eye;

  if ((dir.x == 0 && dir.y == 0 && dir.z == 0) || (m_up.x == 0 && m_up.y == 0 && m_up.z == 0)) {
    return false;
  }

  glm::vec3 toRight = cross(dir, m_up);

  glm::vec3 up1 = cross(toRight, dir);

  up1 /= glm::length(up1);
  dir /= glm::length(dir);

  glm::vec3 dirNew = dir * cosf(angle) + up1 * sinf(angle);

  GLfloat distance = glm::distance(m_center, m_eye);

  glm::vec3 upNew = cross(toRight, dirNew);

  m_center = m_eye + dirNew * distance;
  m_up = upNew;

  return true;
}
