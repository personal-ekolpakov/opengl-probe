//
// Created by pompei on 24.09.23.
//

#ifndef OPENGL_PROBE_UTIL_H
#define OPENGL_PROBE_UTIL_H

#include <cmath>
#include <string>
#include <ostream>
#include <glm/glm.hpp>

#define K_FREE(mode) (((GLFW_MOD_SHIFT|GLFW_MOD_CONTROL|GLFW_MOD_ALT|GLFW_MOD_SUPER) & (mode)) == 0)
#define K_CTRL(mode) (((GLFW_MOD_SHIFT|GLFW_MOD_CONTROL|GLFW_MOD_ALT|GLFW_MOD_SUPER) & (mode)) == GLFW_MOD_CONTROL)
#define K_HAPPENED(action) ((action) == GLFW_PRESS || (action) == GLFW_REPEAT)
#define K_PRESSED(action) ((action) == GLFW_PRESS)

#define M_DEGf (M_PIf/180.0f)
#define M_DEG (M_PI/180.0)

#define GLfloat_OFFSET(i)   (      (GLvoid*)    (  (i)*sizeof(GLfloat)   )      )

std::string mouseButton_ToStr(int mouseButton);

std::string modifiersKeys_ToStr(int mods);

std::string eventAction_ToStr(int action);

template<typename MyTraits>
inline std::basic_ostream<char, MyTraits> &
operator<<(std::basic_ostream<char, MyTraits> &out, const glm::vec2 &v) {
  return out << "vec2(" << v.x << ", " << v.y << ")";
}

/**
 * @brief Rotates camera parameter on angle `angle` radians to right side
 * @param m_eye camera eye position
 * @param m_center position, camera looking to
 * @param m_up camera up direction
 * @param angle angle to rotate to in radians
 * @return true - if operation successful, false - no operation
 */
bool camera_rotate_right(glm::vec3 &m_eye, glm::vec3 &m_center, glm::vec3 &m_up, GLfloat angle);

/**
 * @brief Rotates camera parameter on angle `angle` radians to up side
 * @param m_eye camera eye position
 * @param m_center position, camera looking to
 * @param m_up camera up direction
 * @param angle angle to rotate to in radians
 * @return true - if operation successful, false - no operation
 */
bool camera_rotate_up(glm::vec3 &m_eye, glm::vec3 &m_center, glm::vec3 &m_up, GLfloat angle);

#endif //OPENGL_PROBE_UTIL_H
