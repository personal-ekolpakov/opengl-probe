//
// Created by pompei on 17.09.23.
//

#ifndef OPENGL_PROBE_CAMERA_H
#define OPENGL_PROBE_CAMERA_H

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

enum CameraMove {
  CAMERA_MOVE_RIGHT,
  CAMERA_MOVE_LEFT,
  CAMERA_MOVE_FORWARD,
  CAMERA_MOVE_BACKWARD,
  CAMERA_ROTATE_RIGHT,
  CAMERA_ROTATE_LEFT,
  CAMERA_ROTATE_UP,
  CAMERA_ROTATE_DOWN,
  CAMERA_ROLL_RIGHT,
  CAMERA_ROLL_LEFT,
};

class Camera {
private:

  glm::vec3 m_eye;
  glm::vec3 m_center;
  glm::vec3 m_up;

  GLfloat m_move_step;
  GLfloat m_rotate_angle_step;

  GLfloat m_rotateRightAngle;
  GLfloat m_rotateUpAngle;
  bool m_useRotateAngles;

  int m_viewportWidth, m_viewportHeight;

  GLfloat projection_fovY_grad;
  GLfloat projection_zNear;
  GLfloat projection_zFar;

  void MoveRight(GLfloat step);

  void MoveForward(GLfloat step);

  void RotateUp(GLfloat angle);

  void RotateRight(GLfloat angle);

  void RollRight(GLfloat angle);

public:
  Camera() : m_eye(1.0f),
             m_center(1.0f),
             m_up(1.0f),
             m_move_step(1.0),

             projection_fovY_grad(45.0f),
             projection_zNear(0.1f),
             projection_zFar(100.0f),

             m_rotate_angle_step(glm::radians(10.0f)),
             m_rotateRightAngle(0),
             m_rotateUpAngle(0),
             m_useRotateAngles(false),
             m_viewportWidth(0),
             m_viewportHeight(0) {}

  void LookAt(glm::vec3 eye, glm::vec3 center, glm::vec3 up);

  glm::mat4 GetView();

  glm::mat4 GetProjection();

  void Move(CameraMove cameraMove);

  void UseRotation(GLfloat rotateRightAngle, GLfloat rotateUpAngle);

  void CommitRotation();

  void SetViewportSize(int viewportWidth, int viewportHeight);

  [[nodiscard]] GLfloat GetViewportAspect() const;
};


#endif //OPENGL_PROBE_CAMERA_H
