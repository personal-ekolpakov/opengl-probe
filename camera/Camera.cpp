//
// Created by pompei on 17.09.23.
//
#include <cmath>
#include "Camera.h"
#include "util.h"

void Camera::LookAt(glm::vec3 eye, glm::vec3 center, glm::vec3 up) {
  m_eye = eye;
  m_center = center;
  m_up = up;
}

glm::mat4 Camera::GetView() {

  glm::vec3 eye(m_eye), center(m_center), up(m_up);

  if (m_useRotateAngles) {
    camera_rotate_right(eye, center, up, m_rotateRightAngle);
    camera_rotate_up(eye, center, up, m_rotateUpAngle);
  }

  return glm::lookAt(eye, center, up);
}

void Camera::Move(CameraMove cameraMove) {
  switch (cameraMove) {
    case CAMERA_MOVE_LEFT:
      MoveRight(-m_move_step);
      return;

    case CAMERA_MOVE_RIGHT:
      MoveRight(+m_move_step);
      return;

    case CAMERA_MOVE_FORWARD:
      MoveForward(+m_move_step);
      return;

    case CAMERA_MOVE_BACKWARD:
      MoveForward(-m_move_step);
      return;

    case CAMERA_ROTATE_UP:
      RotateUp(+m_rotate_angle_step);
      return;

    case CAMERA_ROTATE_DOWN:
      RotateUp(-m_rotate_angle_step);
      return;

    case CAMERA_ROTATE_LEFT:
      RotateRight(-m_rotate_angle_step);
      return;

    case CAMERA_ROTATE_RIGHT:
      RotateRight(+m_rotate_angle_step);
      return;

    case CAMERA_ROLL_RIGHT:
      RollRight(+m_rotate_angle_step);
      return;

    case CAMERA_ROLL_LEFT:
      RollRight(-m_rotate_angle_step);
      return;

  }
}

void Camera::MoveRight(GLfloat step) {
  glm::vec3 dir = m_center - m_eye;
  glm::vec3 toLeft = cross(dir, m_up);

  if (toLeft.x == 0 && toLeft.y == 0 && toLeft.z == 0) {
    return;
  }

  glm::vec3 delta = toLeft * (step / glm::length(toLeft));

  m_eye += delta;
  m_center += delta;
}

void Camera::MoveForward(GLfloat step) {
  glm::vec3 dir = m_center - m_eye;

  if (dir.x == 0 && dir.y == 0 && dir.z == 0) {
    return;
  }

  glm::vec3 delta = dir * (step / glm::length(dir));

  m_eye += delta;
  m_center += delta;
}

void Camera::RotateUp(GLfloat angle) {
  camera_rotate_up(m_eye, m_center, m_up, angle);
}

void Camera::RotateRight(GLfloat angle) {
  camera_rotate_right(m_eye, m_center, m_up, angle);
}

void Camera::RollRight(GLfloat angle) {
  glm::vec3 dir = m_center - m_eye;

  if ((dir.x == 0 && dir.y == 0 && dir.z == 0) || (m_up.x == 0 && m_up.y == 0 && m_up.z == 0)) {
    return;
  }

  glm::vec3 toRight = cross(dir, m_up);

  dir /= glm::length(dir);
  toRight /= glm::length(toRight);

  glm::vec3 up1 = cross(toRight, dir);
  up1 /= glm::length(up1);

  glm::vec3 upNew = up1 * cosf(angle) + toRight * sinf(angle);

  m_up = upNew;
}

void Camera::UseRotation(GLfloat rotateRightAngle, GLfloat rotateUpAngle) {
  m_rotateRightAngle = rotateRightAngle;
  m_rotateUpAngle = rotateUpAngle;
  m_useRotateAngles = true;
}

void Camera::CommitRotation() {
  RotateRight(m_rotateRightAngle);
  RotateUp(m_rotateUpAngle);
  m_useRotateAngles = false;
}

void Camera::SetViewportSize(int viewportWidth, int viewportHeight) {
  m_viewportWidth = viewportWidth;
  m_viewportHeight = viewportHeight;
}

GLfloat Camera::GetViewportAspect() const {
  if (m_viewportWidth == 0 || m_viewportHeight == 0) return 1.0f;
  return (GLfloat) m_viewportWidth / (GLfloat) m_viewportHeight;
}

glm::mat4 Camera::GetProjection() {

  glm::mat4 projection(1.0f);
  projection = glm::perspective(45.0f, GetViewportAspect(), 0.1f, 100.0f);

  return projection;
}
