cmake_minimum_required(VERSION 3.26)
project(opengl_probe)
include(cmake_lib/non-invasive-embed-resources.cmake)

set(CMAKE_CXX_STANDARD 17)

find_package(glfw3 3.3 REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(GLUT REQUIRED)

include_directories(${GLEW_INCLUDE_DIRS})
include_directories(${CMAKE_SOURCE_DIR}/SOIL/include)
include_directories(${CMAKE_SOURCE_DIR}/GLM/include)
include_directories(${CMAKE_SOURCE_DIR}/errors)
include_directories(${CMAKE_SOURCE_DIR}/shaders)
include_directories(${CMAKE_SOURCE_DIR}/textures)
include_directories(${CMAKE_SOURCE_DIR}/window)
include_directories(${CMAKE_SOURCE_DIR}/camera)
include_directories(${CMAKE_SOURCE_DIR}/util)
include_directories(${CMAKE_SOURCE_DIR}/state_mode)
include_directories(${CMAKE_SOURCE_DIR}/draw)

link_directories(${CMAKE_SOURCE_DIR}/SOIL/lib)

add_executable(probe_next launchers/probe_next.cpp
    shaders/Shader.cpp shaders/Shader.h
    errors/Errors.cpp errors/Errors.h
    textures/Texture.h
    textures/Texture.cpp
    camera/Camera.h
    camera/Camera.cpp
    window/Window.cpp
    window/Window.h
    util/util.cpp
    util/util.h
    state_mode/StateMode.cpp
    state_mode/StateMode.h
    state_mode/StateMode_MouseWASD.cpp
    state_mode/StateMode_MouseWASD.h
    draw/Draw.cpp
    draw/Draw.h
    draw/Draw_FirstProbe.cpp
    draw/Draw_FirstProbe.h
    draw/Draw_House.cpp
    draw/Draw_House.h
)
target_link_libraries(probe_next glfw)
target_link_libraries(probe_next OpenGL::GL)
target_link_libraries(probe_next OpenGL::GLU)
target_link_libraries(probe_next GLEW::GLEW)
target_link_libraries(probe_next GLUT::GLUT)
target_link_libraries(probe_next ${GLUT_LIBRARY})
target_link_libraries(probe_next ${CMAKE_SOURCE_DIR}/SOIL/lib/libSOIL.a)


embed_resources(probe_next
    shaders/prism_1_vertex.glsl
    shaders/prism_3_fragment.glsl
    
    shaders/house_1_vertex.glsl
    shaders/house_3_fragment.glsl
    shaders/house_2_geo.glsl

    textures/wood_container.jpg
    textures/awesome_face.png
)
