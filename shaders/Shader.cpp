//
// Created by pompei on 14.09.23.
//

#include "Shader.h"

Shader::Shader(const GLchar *vertexSource, const GLchar *geometrySource, const GLchar *fragmentSource) {
  GLuint vertex, fragment, geometry;
  GLint success;
  const int infoLogSize = 1024 * 2;
  GLchar infoLog[infoLogSize];

  bool hasGeometry = geometrySource != nullptr;

  {// Вершинный шейдер
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vertexSource, nullptr);
    glCompileShader(vertex);
    // Если есть ошибки - вывести их
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if (!success) {
      glGetShaderInfoLog(vertex, infoLogSize, nullptr, infoLog);
      std::cerr << "06fzJKmEIR :: ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
  }
  if (hasGeometry) {// Геометрический шейдер
    geometry = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometry, 1, &geometrySource, nullptr);
    glCompileShader(geometry);
    // Если есть ошибки - вывести их
    glGetShaderiv(geometry, GL_COMPILE_STATUS, &success);
    if (!success) {
      glGetShaderInfoLog(geometry, infoLogSize, nullptr, infoLog);
      std::cerr << "fXsJxjj97o :: ERROR::SHADER::GEOMETRY::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
  }
  {// Фрагментный шейдер
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fragmentSource, nullptr);
    glCompileShader(fragment);
    // Если есть ошибки - вывести их
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if (!success) {
      glGetShaderInfoLog(fragment, infoLogSize, nullptr, infoLog);
      std::cerr << "T49iiwV5c9 :: ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
  }

  Program = glCreateProgram();
  glAttachShader(Program, vertex);
  glAttachShader(Program, fragment);
  if (hasGeometry) {
    glAttachShader(Program, geometry);
  }
  glLinkProgram(Program);

  //Если есть ошибки - вывести их
  glGetProgramiv(Program, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(Program, infoLogSize, nullptr, infoLog);
    std::cout << "RUYBa0SvsN :: ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
  }

  // Удаляем шейдеры, поскольку они уже в программу и нам больше не нужны.
  glDeleteShader(vertex);
  glDeleteShader(fragment);
  if (hasGeometry) {
    glDeleteShader(geometry);
  }
}

void Shader::Use() const {
  glUseProgram(Program);
}

GLint Shader::GetUniformLocation(const std::string &uniformVarName) const {
  return glGetUniformLocation(Ref(), uniformVarName.c_str());
}
