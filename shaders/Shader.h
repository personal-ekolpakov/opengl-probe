//
// Created by pompei on 14.09.23.
//

#ifndef OPENGL_PROBE_SHADER_H
#define OPENGL_PROBE_SHADER_H


#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h> // Подключаем glew для того, чтобы получить все необходимые заголовочные файлы OpenGL

class Shader {

private:
  GLuint Program;

public:
  Shader(const GLchar *vertexSource, const GLchar *geometrySource, const GLchar *fragmentSource);

  void Use() const;

  [[nodiscard]] GLuint Ref() const {
    return Program;
  }

  [[nodiscard]] GLint GetUniformLocation(const std::string &uniformVarName) const;

};

#endif //OPENGL_PROBE_SHADER_H
