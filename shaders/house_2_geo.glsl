#version 330 core
layout (points) in;
layout (line_strip, max_vertices = 40) out;

out vec3 normal;

uniform float R;
uniform uint N1;
uniform uint N2;

void main() {

  for (uint i = 0u; i <= N1; i++) {

    float fi = float(i) * 2.0f * 3.1415926f / float(N1);
    float dx = R * cos(fi);
    float dy = R * sin(fi);

    gl_Position = gl_in[0].gl_Position + vec4(dx, dy, 0.0, 0.0);
    normal = vec3(cos(fi), sin(fi), (cos(fi) + sin(fi))/2);
    EmitVertex();
  }


  EndPrimitive();
}
