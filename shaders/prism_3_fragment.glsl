#version 330 core
in vec3 goColor;
in vec2 TexCoord;

out vec4 color;

uniform vec4 ourColor;

uniform sampler2D ourTexture1;
uniform sampler2D ourTexture2;

void main()
{
  vec2 pos = vec2(TexCoord.x, 1 - TexCoord.y);
  //@formatter:off
  color = mix(
             texture( ourTexture1, pos ),
             texture( ourTexture2, pos ),
             ourColor.x
          );
  //@formatter:on
}
