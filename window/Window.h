//
// Created by pompei on 17.09.23.
//

#ifndef OPENGL_PROBE_WINDOW_H
#define OPENGL_PROBE_WINDOW_H

#include <GLFW/glfw3.h>
#include <string>
#include "Camera.h"

class Window;

#include "StateMode.h"

class Window {
private:
  GLFWwindow *m_window;
  
  int m_viewportWidth, m_viewportHeight;

  Camera *m_associated_camera;

  StateMode *m_state_mode;

  void keyCallback(int key, int scancode, int action, int mode);

  void clearStateMode();
  
  void startStateMode_MouseWASD();
  
  friend StateMode;
  
  void commitStateMode();

  void mouseButtonCallback(int button, int action, int mods);

  void mouseMoveCallback(glm::vec2 mousePos);

  void windowSizeCallback();

public:
  Window() : m_window(nullptr), m_associated_camera(nullptr), m_state_mode(nullptr), m_viewportWidth(0), m_viewportHeight(0) {}

  ~Window();

  void Create(int width, int height, const std::string &title);

  void MakeCurrentContext();

  void GetFramebufferSize(int *width, int *height);

  bool ShouldClose();

  void SwapBuffers();

  void AssociateWithCamera(Camera *camera);

  void AssociatedCameraMove(CameraMove cameraMove);

  void UpdateViewportSize();

};


#endif //OPENGL_PROBE_WINDOW_H
