//
// Created by pompei on 17.09.23.
//

#include <iostream>
#include "Window.h"
#include "util.h"
#include "StateMode_MouseWASD.h"

void Window::Create(int width, int height, const std::string &title) {
  m_window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
  if (!m_window) {
    throw std::runtime_error("I4A3pcg1xj :: i cannot create window");
  }

  glfwSetWindowUserPointer(m_window, this);
  glfwSetKeyCallback(m_window, [](GLFWwindow *window, int key, int scancode, int action, int mode) {
    void *userPointer = glfwGetWindowUserPointer(window);
    ((Window *) userPointer)->keyCallback(key, scancode, action, mode);
  });

  glfwSetMouseButtonCallback(m_window, [](GLFWwindow *window, int button, int action, int mods) {
    void *userPointer = glfwGetWindowUserPointer(window);
    ((Window *) userPointer)->mouseButtonCallback(button, action, mods);
  });

  glfwSetCursorPosCallback(m_window, [](GLFWwindow *window, double posX, double posY) {
    void *userPointer = glfwGetWindowUserPointer(window);
    ((Window *) userPointer)->mouseMoveCallback(glm::vec2(posX, posY));
  });

  glfwSetWindowSizeCallback(m_window, [](GLFWwindow *window, int width, int height) {
    void *userPointer = glfwGetWindowUserPointer(window);
    ((Window *) userPointer)->windowSizeCallback();
  });
}

void Window::MakeCurrentContext() {
  glfwMakeContextCurrent(m_window);
}

void Window::keyCallback(int key, int scancode, int action, int mode) {
  if (m_state_mode) {
    bool performed = m_state_mode->KeyCallback(key, scancode, action, mode);
    if (performed) return;
  }

  if (action == GLFW_PRESS && K_CTRL(mode) && key == GLFW_KEY_X) {
    glfwSetWindowShouldClose(m_window, GL_TRUE);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_LEFT) {
    AssociatedCameraMove(CAMERA_ROTATE_LEFT);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_A) {
    AssociatedCameraMove(CAMERA_MOVE_LEFT);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_D) {
    AssociatedCameraMove(CAMERA_MOVE_RIGHT);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_RIGHT) {
    AssociatedCameraMove(CAMERA_ROTATE_RIGHT);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_W) {
    AssociatedCameraMove(CAMERA_MOVE_FORWARD);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_UP) {
    AssociatedCameraMove(CAMERA_ROTATE_UP);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_S) {
    AssociatedCameraMove(CAMERA_MOVE_BACKWARD);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_DOWN) {
    AssociatedCameraMove(CAMERA_ROTATE_DOWN);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_E) {
    AssociatedCameraMove(CAMERA_ROLL_RIGHT);
    return;
  }
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_Q) {
    AssociatedCameraMove(CAMERA_ROLL_LEFT);
    return;
  }
  if (K_FREE(mode) && key == GLFW_KEY_C) {
    if (K_PRESSED(action)) {
      startStateMode_MouseWASD();
      return;
    }
    return;
  }
  if (action == GLFW_PRESS) {
    std::cout << "vvn35FWT7H :: PRESS   key = " << key << ", scancode = " << scancode << std::endl;
  } else if (action == GLFW_RELEASE) {
    std::cout << "eqaVRBs2Ud :: RELEASE key = " << key << ", scancode = " << scancode << std::endl;
  } else if (action == GLFW_REPEAT) {
    std::cout << "uPok5xKtgC :: REPEAT  key = " << key << ", scancode = " << scancode << std::endl;
  }
}

void Window::GetFramebufferSize(int *width, int *height) {
  glfwGetFramebufferSize(m_window, width, height);
}

bool Window::ShouldClose() {
  return glfwWindowShouldClose(m_window);
}

void Window::SwapBuffers() {
  glfwSwapBuffers(m_window);
}

void Window::AssociateWithCamera(Camera *camera) {
  m_associated_camera = camera;
}

void Window::clearStateMode() {
  if (m_state_mode == nullptr) {
    return;
  }
  delete m_state_mode;
  m_state_mode = nullptr;
}

Window::~Window() {
  clearStateMode();
}

void Window::AssociatedCameraMove(CameraMove cameraMove) {
  Camera *c = m_associated_camera;
  if (c) {
    commitStateMode();
    c->Move(cameraMove);
  }
}

void Window::mouseButtonCallback(int button, int action, int mods) {
  if (m_state_mode) {
    bool performed = m_state_mode->MouseButtonCallback(button, action, mods);
    if (performed) return;
  }

  std::cout << "OP888GVxq2 :: mouse callback from Window "
            << mouseButton_ToStr(button)
            << " " << modifiersKeys_ToStr(mods)
            << " " << eventAction_ToStr(action)
            << std::endl;
}

void Window::mouseMoveCallback(glm::vec2 mousePos) {
  if (m_state_mode) {
    bool performed = m_state_mode->MouseMoveCallback(mousePos);
    if (performed) return;
  }

  // do self operations
}

void Window::startStateMode_MouseWASD() {
  clearStateMode();

  std::cout << "DDj45U69xS :: startStateMode_MouseWASD" << std::endl;

  Camera *camera = m_associated_camera;
  if (!camera) return;

  m_state_mode = dynamic_cast<StateMode *>(new StateMode_MouseWASD(this, camera));

  m_state_mode->Initialize();
}

void Window::commitStateMode() {
  StateMode *sm = m_state_mode;
  if (sm) sm->Commit();
}

void Window::windowSizeCallback() {
  UpdateViewportSize();
}

void Window::UpdateViewportSize() {
  GetFramebufferSize(&m_viewportWidth, &m_viewportHeight);
  glViewport(0, 0, m_viewportWidth, m_viewportHeight);
  if (m_associated_camera != nullptr) {
    m_associated_camera->SetViewportSize(m_viewportWidth, m_viewportHeight);
  } else {
    std::cerr << "Y1kZUP0Y2L :: Нет камеры при изменении размера окна визуализации" << std::endl;
  }
}
