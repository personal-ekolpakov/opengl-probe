//
// Created by pompei on 24.09.23.
//

#include "StateMode.h"
#include "util.h"

StateMode::StateMode(Window *window) : m_window(window) {}

GLFWwindow *StateMode::getNativeWindow() {
  return m_window->m_window;
}

bool StateMode::KeyCallback(int key, int scancode, int action, int mode) {
  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_ESCAPE) {
    TurnOffThisMode();
    return true;
  }
  return false;
}

void StateMode::TurnOffThisMode() {
  m_window->clearStateMode();
}

StateMode::~StateMode() = default;
