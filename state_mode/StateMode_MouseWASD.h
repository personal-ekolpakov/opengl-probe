//
// Created by pompei on 24.09.23.
//

#ifndef OPENGL_PROBE_STATE_MODE_MOUSE_WASD_H
#define OPENGL_PROBE_STATE_MODE_MOUSE_WASD_H

#include "StateMode.h"
#include <GLFW/glfw3.h>

class StateMode_MouseWASD : public StateMode {
private:
  Camera *m_camera;
  glm::vec2 m_mouseStartPos;
  bool m_mouseStartPosInitiated;

  void Initialize() override;

  void setNativeWindowMode(int window_mode_new);

public:
  explicit StateMode_MouseWASD(Window *window, Camera *camera);

  ~StateMode_MouseWASD() override;

  bool MouseButtonCallback(int button, int action, int mods) override;

  bool MouseMoveCallback(glm::vec2 mousePos) override;

  bool KeyCallback(int key, int scancode, int action, int mode) override;

  void TurnOffThisMode() override;

  void Commit() override;
};


#endif //OPENGL_PROBE_STATE_MODE_MOUSE_WASD_H
