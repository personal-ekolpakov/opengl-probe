//
// Created by pompei on 24.09.23.
//

#include <iostream>
#include <GLFW/glfw3.h>
#include "StateMode_MouseWASD.h"
#include "util.h"

StateMode_MouseWASD::StateMode_MouseWASD(Window *window, Camera *camera) : StateMode(window),
                                                                           m_camera(camera),
                                                                           m_mouseStartPos(),
                                                                           m_mouseStartPosInitiated(false) {}

StateMode_MouseWASD::~StateMode_MouseWASD() = default;

void StateMode_MouseWASD::Initialize() {
  setNativeWindowMode(GLFW_CURSOR_DISABLED);
}

bool StateMode_MouseWASD::MouseButtonCallback(int button, int action, int mods) {
  return false;
}

bool StateMode_MouseWASD::MouseMoveCallback(glm::vec2 mousePos) {
  if (!m_mouseStartPosInitiated) {
    m_mouseStartPosInitiated = true;
    m_mouseStartPos = mousePos;
    return true;
  }

  glm::vec2 delta = mousePos - m_mouseStartPos;

  GLfloat angleRight = +(GLfloat) delta.x * M_DEGf * 0.5f;
  GLfloat angleUp    = -(GLfloat) delta.y * M_DEGf * 0.5f;

  m_camera->UseRotation(angleRight, angleUp);

  return true;
}

bool StateMode_MouseWASD::KeyCallback(int key, int scancode, int action, int mode) {
  if (StateMode::KeyCallback(key, scancode, action, mode)) {
    return true;
  }

  if (K_HAPPENED(action) && K_FREE(mode) && key == GLFW_KEY_C) {
    TurnOffThisMode();
    return true;
  }

  return false;
}

void StateMode_MouseWASD::TurnOffThisMode() {
  Commit();
  setNativeWindowMode(GLFW_CURSOR_NORMAL);
  StateMode::TurnOffThisMode();
}

void StateMode_MouseWASD::setNativeWindowMode(int window_mode_new) {
  int window_mode = glfwGetInputMode(getNativeWindow(), GLFW_CURSOR);
  if (window_mode != window_mode_new) {
    glfwSetInputMode(getNativeWindow(), GLFW_CURSOR, window_mode_new);
  }
}

void StateMode_MouseWASD::Commit() {
  m_mouseStartPosInitiated = false;
  m_camera->CommitRotation();
}
