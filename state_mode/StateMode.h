//
// Created by pompei on 24.09.23.
//

#ifndef OPENGL_PROBE_STATE_MODE_H
#define OPENGL_PROBE_STATE_MODE_H

class StateMode;

#include "Window.h"

class StateMode {
private:
  Window *m_window;

protected:
  GLFWwindow *getNativeWindow();

public:
  explicit StateMode(Window *window);

  virtual ~StateMode();

  virtual void Initialize() = 0;

  virtual bool MouseButtonCallback(int button, int action, int mods) = 0;

  virtual bool MouseMoveCallback(glm::vec2 mousePos) = 0;

  virtual bool KeyCallback(int key, int scancode, int action, int mode);

  virtual void TurnOffThisMode();
  
  virtual void Commit() = 0;
};

#endif //OPENGL_PROBE_STATE_MODE_H
