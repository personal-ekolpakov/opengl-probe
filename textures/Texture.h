//
// Created by pompei on 17.09.23.
//

#ifndef OPENGL_PROBE_TEXTURE_H
#define OPENGL_PROBE_TEXTURE_H

#include <GL/glew.h> // Подключаем glew для того, чтобы получить все необходимые заголовочные файлы OpenGL
#include <string>
#include "Shader.h"

class Texture {
private:
  GLuint m_texture_ref;
  int tex_width, tex_height;

  Texture(const std::string &placeId, const unsigned char *image_in_memory, int image_size_bytes);

public:
  static Texture *get_wood_container();

  static Texture *get_awesome_face();

  GLuint Ref() {
    return m_texture_ref;
  }

  void Bind(Shader *shader, GLint textureBondIndex, const std::string& shaderUniformVarName);
};

#endif //OPENGL_PROBE_TEXTURE_H
