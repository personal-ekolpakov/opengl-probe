//
// Created by pompei on 17.09.23.
//

#include "Texture.h"
#include "Errors.h"
#include <SOIL.h>
#include <iostream>

extern "C" const unsigned char textures_wood_container_jpg[];
extern "C" const int textures_wood_container_jpg_size;
extern "C" const unsigned char textures_awesome_face_png[];
extern "C" const int textures_awesome_face_png_size;

static Texture *wood_container = nullptr;
static Texture *awesome_face = nullptr;

Texture *Texture::get_wood_container() {
  return wood_container
         ? wood_container
         : wood_container = new Texture("zrrIPdrRPc", &textures_wood_container_jpg[0], textures_wood_container_jpg_size);
}

Texture *Texture::get_awesome_face() {
  return awesome_face
         ? awesome_face
         : awesome_face = new Texture("Kx1yJY4QRG", &textures_awesome_face_png[0], textures_awesome_face_png_size);
}

Texture::Texture(const std::string &placeId,
                 const unsigned char *const image_in_memory,
                 const int image_size_bytes) : m_texture_ref(0), tex_width(0), tex_height(0) {

  glGenTextures(1, &m_texture_ref);
  glBindTexture(GL_TEXTURE_2D, m_texture_ref);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  unsigned char *image = SOIL_load_image_from_memory(image_in_memory,
                                                     image_size_bytes,
                                                     &tex_width,
                                                     &tex_height,
                                                     nullptr,
                                                     SOIL_LOAD_RGB);

  if (!image) {
    throw std::runtime_error(placeId + " Wa1jI9aJK4 :: Error SOIL_load_image_from_memory() returns nullptr");
  }

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex_width, tex_height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
  Errors::checkGlError("cErw2kD4bv");
  glGenerateMipmap(GL_TEXTURE_2D);
  SOIL_free_image_data(image);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::Bind(Shader *shader, GLint textureBondIndex, const std::string& shaderUniformVarName) {

  GLint varNameLocationIndex = glGetUniformLocation(shader->Ref(), shaderUniformVarName.c_str());
  
  glActiveTexture(GL_TEXTURE0 + textureBondIndex);
  glBindTexture(GL_TEXTURE_2D, Ref());
  glUniform1i(varNameLocationIndex, textureBondIndex);
}

